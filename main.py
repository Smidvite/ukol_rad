from __future__ import print_function 
import random
import time
from contextlib import contextmanager
import sys
from multiprocessing import Manager, Pool

def findKParalell(results, array,indexFrom,indexTo,k):
    results.append(findK(array,indexFrom,indexTo,k))

def findK(array,indexFrom,indexTo,k): 
    for i in range(indexFrom,indexTo):
        elementin = array[i]
        smallernumber = 0
        samenumber = 0
        for j in range(len(array)):
            if(array[j]<elementin):
                smallernumber+=1
            if(array[j]==elementin):
                samenumber+=1
        while(samenumber!=0):
            samenumber-=1
            val = smallernumber + samenumber 
            if(val == k):
                return elementin
    return None

@contextmanager
def process_pool(size):
    pool = Pool(size)
    yield pool
    pool.close()
    pool.join()


def findparralelktyprvek(array, process_count,k):
    print("Using ",process_count, "parallel processes")
    length = len(array)
    step = int(length / process_count)

    manager = Manager()
    results = manager.list()

    with process_pool(size=process_count) as pool:
        for n in range(process_count):             
            if n < process_count - 1:
                indexFrom = n * step
                indexTo = (n + 1) * step
            else:
                indexFrom = n * step
                indexTo = len(array)
            pool.apply_async(findKParalell, (results, array,indexFrom,indexTo,k))

    for res in results:
        if(res != None):
            return res
    return None

def findM(array):
    return max(array)

def findMaxParalell(results,array):
    results.append(findM(array))

def findparalernemaximalniprvek(array, process_count):
    print("Using ",process_count, "parallel processes")
    length = len(array)
    step = int(length / process_count)
    manager = Manager()
    results = manager.list()

    with process_pool(size=process_count) as pool:
        for n in range(process_count):
            if n < process_count - 1:
                chunk = array[n * step:(n + 1) * step]
            else:
                chunk = array[n * step:]
            pool.apply_async(findMaxParalell, (results, chunk))

    return max(results)

def vratKtyNemensiPrvek(array,numberOfParallelProcesses,k):
    pole = findparralelktyprvek(array, numberOfParallelProcesses,k)
    return pole

def vratMax(array,numberOfParallelProcesses):
    pole = findparalernemaximalniprvek(array, numberOfParallelProcesses)
    return pole


if __name__ == '__main__':
    nejprv = 5
    randomized_array = [random.randint(0, n * 10) for n in range(10)]    
    prvninejprvek = vratKtyNemensiPrvek(randomized_array,4,nejprv)
    print("Nejmenší prvek",prvninejprvek)
    druhejnejprvek = vratMax(randomized_array,4)
    print("Nejvetší prvek ",druhejnejprvek)
 
