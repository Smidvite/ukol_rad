from __future__ import print_function 
import random
import time
from contextlib import contextmanager
import sys
from multiprocessing import Manager, Pool

def findKParalell(results, array,indexFrom,indexTo,k):
    results.append(findK(array,indexFrom,indexTo,k))

def findK(array,indexFrom,indexTo,k): 
    for i in range(indexFrom,indexTo):  # prochází index od do
        elementin = array[i] # předá hodnotu z pole
        smallernumber = 0 # definuje malé číslo
        samenumber = 0 # ruzné číslo
        for j in range(len(array)): # prochází celé pole
            if(array[j]<elementin): # porovná hodnoty z pole když je elementin menší 
                smallernumber+=1    # přidá hodnotu 
            if(array[j]==elementin): # když se rovná
                samenumber+=1 # přidá plus jedna 
        while(samenumber!=0): # dokud číslo  je ruzné od nuly
            samenumber-=1	  # odeberu rozdilné číslo
            val = smallernumber + samenumber  # spočítá hodnotu ktého nejmenšího prvku 
            if(val == k):
                return elementin  # vrátí maximum
    return None

@contextmanager
def process_pool(size):

	# Vytvořte procesní fond a blokujte do
   # všechny procesy byly dokončeny.
   # Poznámka: viz také concurrent.futures.ProcessPoolExecutor
    
	pool = Pool(size)
    yield pool
    pool.close()
    pool.join()


def findparralelktyprvek(array, process_count,k):
    print("Using ",process_count, "parallel processes") # process_count = počet paralerních procesů
    length = len(array) # celkový počet hodnot v poli 
    step = int(length / process_count) # výpočet počtu kroků
	
	   # Okamžitý multiprocessing.Manager objekt
    # ukladání výstupů pro jednotlivý proces

    manager = Manager()
    results = manager.list()

    with process_pool(size=process_count) as pool:
	    # vytváříme nový process a objekt 
        # vložím funkci pro k nejmenší prvek
        # použití jako vstup sublist
        for n in range(process_count):             
            if n < process_count - 1:
                indexFrom = n * step
                indexTo = (n + 1) * step
            else:
				# získá zbývající prvky ze seznamu
                indexFrom = n * step
                indexTo = len(array)
            pool.apply_async(findKParalell, (results, array,indexFrom,indexTo,k))

    for res in results:
        if(res != None):
            return res
    return None

def findM(array):
    return max(array)

def findMaxParalell(results,array):
    results.append(findM(array))

def findparalernemaximalniprvek(array, process_count):
    print("Using ",process_count, "parallel processes") # process_count = počet paralerních procesů
    length = len(array) # celkový počet hodnot v poli
    step = int(length / process_count)  # výpočet počtu kroků 
    manager = Manager()
    results = manager.list()


	    # Okamžitý multiprocessing.Manager objekt
    # ukladání výstupů pro jednotlivý proces

    with process_pool(size=process_count) as pool:
	    # vytváříme nový process a objekt 
        # vložím funkci pro maximální hodnotu
        # použití jako vstup sublist
        for n in range(process_count):
            if n < process_count - 1:
                chunk = array[n * step:(n + 1) * step]
            else:
			# získá zbývající prvky ze seznamu
                chunk = array[n * step:]
            pool.apply_async(findMaxParalell, (results, chunk))

    return max(results) # vrátí maximální hodnotu

def vratKtyNemensiPrvek(array,numberOfParallelProcesses,k):
    pole = findparralelktyprvek(array, numberOfParallelProcesses,k)
    return pole # vrátí K nejmenší hodnotu 

def vratMax(array,numberOfParallelProcesses):
    pole = findparalernemaximalniprvek(array, numberOfParallelProcesses)
    return pole # vrátí nejvyžší hodnotu


if __name__ == '__main__':
    nejprv = 5 # Kolikátý prvek v pořadí bude
    randomized_array = [random.randint(0, n * 10) for n in range(10)]    # vygenerování náhodného pole     
    prvninejprvek = vratKtyNemensiPrvek(randomized_array,4,nejprv) # najdi nejmenší prvek z pole, počet paralerních procesů a prvek, který nás zajímá 
    print("Nejmenší prvek",prvninejprvek) # vypiš nejnižší prvek
    druhejnejprvek = vratMax(randomized_array,4) # vypíše nejvyžší prvek z náhodného pole
    print("Nejvetší prvek ",druhejnejprvek) # vypíše nejvyžší prvek
 
